import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hero_basics/hero.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(const Heroes());
}
