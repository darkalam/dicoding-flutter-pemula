import 'package:flutter/material.dart';
import 'package:hero_basics/data/heroes.dart';

import 'answer_button.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class QuestionsScreen extends StatefulWidget {
  const QuestionsScreen({
    Key? key,
    required this.onSelectedAnswer,
  }) : super(key: key);

  final void Function(String heroes) onSelectedAnswer;

  @override
  State<QuestionsScreen> createState() {
    return _QuestionsScreenState();
  }
}

class _QuestionsScreenState extends State<QuestionsScreen> {
  var currentQuestionIndex = 0;

  void answerQuestion(String selectedHero) {
    widget.onSelectedAnswer(selectedHero);
    setState(() {
      currentQuestionIndex++;
    });
  }

  @override
  Widget build(BuildContext context) {
    final currentQuestion = heroes[currentQuestionIndex];

    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            LayoutBuilder(
              builder: (context, constraints) {
                if (kIsWeb || constraints.maxWidth > 600) {
                  return Expanded(
                    child: Image.network(
                      currentQuestion.text,
                      width: 200,
                      alignment: Alignment.center,
                    ),
                  );
                } else {
                  return Image.network(
                    currentQuestion.text,
                    width: 200,
                    alignment: Alignment.center,
                  );
                }
              },
            ),
            const SizedBox(
              height: 30,
            ),
            ...currentQuestion.shuffledAnswers.map(
              (item) {
                return AnswerButton(
                  item,
                  () {
                    answerQuestion(item);
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
