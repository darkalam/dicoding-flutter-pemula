import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hero_basics/data/heroes.dart';
import 'package:hero_basics/question_summary/questions_summary.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class ResultsScreen extends StatelessWidget {
  const ResultsScreen({
    Key? key,
    required this.chooseAnswer,
    required this.onRestart,
  }) : super(key: key);

  final List<String> chooseAnswer;
  final void Function() onRestart;

  List<Map<String, Object>> getSummaryData() {
    final List<Map<String, Object>> summary = [];

    for (var i = 0; i < chooseAnswer.length; i++) {
      summary.add(
        {
          'question_index': i,
          'question': heroes[i].text,
          'correct_answer': heroes[i].answers[0],
          'user_answer': chooseAnswer[i],
        },
      );
    }

    return summary;
  }

  @override
  Widget build(BuildContext context) {
    final summaryData = getSummaryData();
    final numTotalQuestions = heroes.length;
    final numCorrectQuestions = summaryData.where((data) {
      return data['user_answer'] == data['correct_answer'];
    }).length;

    return LayoutBuilder(
      builder: (context, constraints) {
        if (kIsWeb || constraints.maxWidth > 600) {
          return SizedBox(
            width: double.infinity,
            child: SingleChildScrollView(
              child: _buildContent(
                numCorrectQuestions,
                numTotalQuestions,
                summaryData,
              ),
            ),
          );
        } else {
          return _buildContent(
            numCorrectQuestions,
            numTotalQuestions,
            summaryData,
          );
        }
      },
    );
  }

  Widget _buildContent(int numCorrectQuestions, int numTotalQuestions,
      List<Map<String, Object>> summaryData) {
    return Container(
      margin: const EdgeInsets.all(40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'You answered $numCorrectQuestions out of $numTotalQuestions questions correctly!',
            style: GoogleFonts.lato(
              color: const Color.fromARGB(255, 230, 200, 253),
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 30,
          ),
          QuestionsSummary(summaryData),
          const SizedBox(
            height: 30,
          ),
          TextButton.icon(
            onPressed: onRestart,
            style: TextButton.styleFrom(
              foregroundColor: Colors.white,
            ),
            icon: const Icon(Icons.refresh),
            label: const Text('Restart Quiz!'),
          ),
        ],
      ),
    );
  }
}
