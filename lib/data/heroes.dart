import 'package:hero_basics/models/hero_questions.dart';

const heroes = [
  HeroQuestions(
    'https://images.tokopedia.net/img/JFrBQq/2022/7/22/a8607bf6-cab6-46ab-bb26-f7541d3490f7.jpg',
    [
      'Achmad Soebardjo',
      'Soeharto',
      'Ir Soekarno',
      'Bung Tomo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/img/JFrBQq/2022/7/22/c9d9e558-99cc-4623-8dba-3928b0fad458.jpg',
    [
      'Soepomo',
      'Achmad Soebardjo',
      'Ir Soekarno',
      'Bung Tomo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/img/JFrBQq/2022/5/23/c16d724c-60f7-40cb-880c-decef6b7d020.jpg',
    [
      'RM Tirto Adi Soerjo',
      'Soepomo',
      'Achmad Soebardjo',
      'Bung Tomo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/img/JFrBQq/2022/5/23/8f6a30ee-53b2-44a3-adfd-b6f507bfa269.jpg',
    [
      'H.O.S Tjokroaminoto',
      'RM Tirto Adi Soerjo',
      'Soepomo',
      'Achmad Soebardjo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/blog-tokopedia-com/uploads/2019/12/jenderalsoedirman.jpg',
    [
      'Jenderal Soedirman',
      'H.O.S Tjokroaminoto',
      'RM Tirto Adi Soerjo',
      'Soepomo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/blog-tokopedia-com/uploads/2019/12/Hasyim-Asyari.jpg',
    [
      'K.H Hasyim Asyari',
      'H.O.S Tjokroaminoto',
      'RM Tirto Adi Soerjo',
      'Soepomo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/blog-tokopedia-com/uploads/2019/12/pangeran-diponegoro.jpg',
    [
      'Pangeran Diponegoro',
      'K.H Hasyim Asyari',
      'H.O.S Tjokroaminoto',
      'RM Tirto Adi Soerjo',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/blog-tokopedia-com/uploads/2019/12/Ki-Hadjar-Dewantara.jpg',
    [
      'Ki Hadjar Dewantara',
      'Pangeran Diponegoro',
      'K.H Hasyim Asyari',
      'H.O.S Tjokroaminoto',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/blog-tokopedia-com/uploads/2019/12/bung-tomo.jpg',
    [
      'Bung Tomo',
      'Pangeran Diponegoro',
      'K.H Hasyim Asyari',
      'H.O.S Tjokroaminoto',
    ],
  ),
  HeroQuestions(
    'https://images.tokopedia.net/img/JFrBQq/2022/7/25/e14bb548-2677-4ae2-8ced-8fa27146e3b7.jpg',
    [
      'Sultan Hasanuddin',
      'Pangeran Diponegoro',
      'K.H Hasyim Asyari',
      'H.O.S Tjokroaminoto',
    ],
  ),
];
